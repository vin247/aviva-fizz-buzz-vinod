package com.aviva;

import java.text.SimpleDateFormat;
import java.util.Date;

public class BitBucketHelper {

	public StringBuilder validateData(int n) {
		String st = validateInput(n);
		StringBuilder sb = new StringBuilder();
		sb.append(st);
		if (st.length() < 1) {
			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("E");
			String day = sdf.format(date);
			String firstCharOfDay = Character.toString(day.charAt(0)).toLowerCase();
			
			for (int i = 1; i <= n; i++) {
				if (i % 3 == 0 && i % 5 == 0) {
					sb.append(firstCharOfDay).append("izz").append(" ").append(firstCharOfDay).append("uzz").append("\n");
				} else if (i % 3 == 0) {
					sb.append(firstCharOfDay).append("izz").append("\n");
				} else if (i % 5 == 0) {
					sb.append(firstCharOfDay).append("uzz").append("\n");
				} else {
					sb.append(i).append("\n");
				}

			}
			return sb;

		}
		return sb;
	}

	private String validateInput(int n) {
		if (1 < n && n < 1000) {
			return "";
		}
		return "Number should be in between 1 and 1000";
	}

}
