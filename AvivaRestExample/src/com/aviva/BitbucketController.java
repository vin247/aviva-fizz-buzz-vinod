package com.aviva;

import java.util.Scanner;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;


@Path("/verticalList")
public class BitbucketController {
	
	@GET
	@Path("/{number}")
	@Produces(MediaType.TEXT_PLAIN)
	public StringBuilder getVerticalList(@PathParam("number") String number){
		System.out.println("BitbucketController.getVerticalList() ::::" + number);
		BitBucketHelper bitBucketHelper = new BitBucketHelper();
		StringBuilder msg = null;
		msg = bitBucketHelper.validateData(Integer.parseInt(number));
		return msg;
	}
	
	public static void main(String [] arg){
		int num = 100;
		
		Scanner sc = new Scanner(System.in);
		BitBucketHelper bitBucketHelper = new BitBucketHelper();
		int n = sc.nextInt();
		StringBuilder flag = bitBucketHelper.validateData(n);
		System.out.println("BitbucketController.main()::: " + flag);
	}

}
