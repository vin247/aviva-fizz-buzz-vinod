package main.test;

import static org.junit.Assert.fail;

import org.junit.Test;

import com.aviva.BitbucketController;

import junit.framework.Assert;

public class TestBitBucketController {

	@SuppressWarnings("deprecation")
	@Test
	public void testVerticalList() {
		BitbucketController b = new BitbucketController();
		StringBuilder sb = b.getVerticalList("2000");
		Assert.assertEquals("Number should be in between 1 and 1000", sb.toString());
	}
	
	@SuppressWarnings("deprecation")
	@Test
	public void testVerticalListPositiveScenarion() {
		BitbucketController b = new BitbucketController();
		StringBuilder sb = b.getVerticalList("900");
		Assert.assertTrue(sb.length() > 1);
		}

}
